const log4js = require('log4js');
/* log4js.configure({
  appenders: {
    out: { type: 'stdout' },
    app: { type: 'file', filename: 'info.log' }
  },
  categories: {
    default: { appenders: [ 'out', 'app' ], level: 'info' }
  }
}); */

log4js.configure({
  appenders: {
    everything: { type: 'file', filename: 'info.log' },
    emergencies: { type: 'file', filename: 'error.log' },
    errors: { type: 'logLevelFilter', appender: 'emergencies', level: 'error' }
  },
  categories: {
    default: { appenders: ['errors', 'everything' ], level: 'info' }
  }
});

export const logger = log4js.getLogger();