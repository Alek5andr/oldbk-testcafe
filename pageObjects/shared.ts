import {Selector, t} from 'testcafe';
import { logger } from '../log4js.conf';

export class Shared {
    constructor() {}

    async clickElement(selector: Selector) {
        logger.info('  Clicking element with selector:');
        logger.info(selector);
        await t.click(selector);
        return true;
    }

    async clickCheckbox(text) {
        logger.info('  Clicking checkbox with text: ' + text);
        let selector = Selector('input[type="checkbox"]').withText(text);
        return await this.clickElement(selector);
    }

    async typeText(fieldSelector, text) {
        logger.info('  Typing text: ' + text);
        await t.typeText(fieldSelector, text);
    }
}