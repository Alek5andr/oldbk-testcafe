import {Selector, t} from 'testcafe';
import {Shared} from '../pageObjects/shared';
import { logger } from '../log4js.conf';

class UserInfo extends Shared {
    userLabel: Selector;
    newsWindow: Selector;
    newsWindowCloseX: Selector;

    constructor() {
        super();
        this.userLabel = Selector('center>b');
        this.newsWindow = Selector('#actiondiv');
        this.newsWindowCloseX = Selector('img[onclick^="document.getElementById(\'actiondiv\')"]');
    }

    get userLabelSelector(): Selector {
        return this.userLabel;
    }

    get newsWindowSelector(): Selector {
        return this.newsWindow;
    }

    async dismissNews() {
        if(this.newsWindow.visible) {
            logger.info('Dismissing news...');
            return await super.clickElement(this.newsWindowCloseX);
        }
    }

}

export const userInfo = new UserInfo();