import {Selector, Role, t} from 'testcafe';
import {Shared} from '../pageObjects/shared';
import {logger} from '../log4js.conf.js';

class Main extends Shared {
    loginInput: Selector;
    passInput: Selector;
    submitBtn: Selector;

    constructor() {
        super();
        this.loginInput = Selector('#form-enter input[name="login"]');
        this.passInput = Selector('#form-enter input[name="psw"]');
        this.submitBtn = Selector('#form-enter input[type="submit"]');
    }

    async enterUsername(username) {
        if(this.loginInput.exists) {
            logger.info('Typing username: ' + username);
            await super.typeText(this.loginInput, username);
        }
    }

    async enterPass(pass) {
        if(this.passInput.exists) {
            logger.info('Typing pass: ' + pass);
            await super.typeText(this.passInput, pass);
        }
    }

    async logIn() {
        logger.info('Logging in...');
        return await super.clickElement(this.submitBtn);
    }

    async logInWithRole(username, pass) {
        return Role('http://oldbk.com/', async t => {
            await this.enterUsername(username);
            await this.enterPass(pass);
            await this.logIn();
        }, {preserveUrl: true});
    }
}

export const main = new Main();