import {Selector, t} from 'testcafe';
import {Shared} from '../pageObjects/shared';
import {logger} from '../log4js.conf.js';

class RoomActivities extends Shared {
    huntingIcon: Selector;

    constructor() {
        super();
        this.huntingIcon = Selector('img[title="Охота"]');
    }

    async switchRoomActivitiesTo(text) {
        logger.info('Switching to room activities: ' + text);
        switch(text) {
            default:
                throw Error('No such room activity is implemented: ' + text);
            case 'Охота':
                this.roomActivitiesText(text);
                return await super.clickElement(this.huntingIcon);
        }
    }

}

export const roomActivities = new RoomActivities();