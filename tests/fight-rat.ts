import {Selector} from "testcafe";
import {main} from '../pageObjects/main';
import {userInfo} from '../pageObjects/user-info';
import {roomActivities} from '../pageObjects/room-activities';

fixture ('Find rat and fight it').page('http://oldbk.com/');

test('Log in', async t => {
    await t.useRole(await main.logInWithRole(process.env.USER, provess.env.PASS));
    await t.expect(await userInfo.userLabelSelector.visible).ok('Did not log in.');
    t.debug();
}).only;

test('Dismiss news, if any', async t => {
    await t.expect(userInfo.dismissNews()).ok('There were news, but smt was wrong.');
    await t.expect(userInfo.newsWindowSelector.visible).notOk('News were not dismissed.');
    t.debug();
});

test('Select hunting mode', async t => {
    const checkboxLabel = 'Обновлять автомат.';
    await t.expect(roomActivities.clickCheckbox(checkboxLabel)).ok('Checkbox "' + checkboxLabel + '" was not clicked.');
    await t.expect(roomActivities.switchRoomActivitiesTo('Охота')).ok('Room activities were not swiched.');
    t.debug();
});
